# nfsmwai

*work in progress*

A competitive time trial AI for Need for Speed: Most Wanted (2005) (NFS:MW) using Deep Reinforcement Learning.


## Installation

Code and software only runs on Windows, and is developed on Windows 10. <3 Linux, but the game runs natively on Windows, as do gamepads and virtual gamepads.

1. Install Need for Speed Most Wanted (2005)
1. Install Patch v1.3 for Need for Speed Most Wanted (2005)
1. Install [NFSMW Extra Options](https://github.com/ExOptsTeam/NFSMWExOpts/releases) (v10.0.0.1338)
1. Copy configuration for [NFSMW Extra Options](https://github.com/ExOptsTeam/NFSMWExOpts/releases) (`extra/NFSMWExtraOptionsSettings.ini`) to `C:\Program Files (x86)\EA GAMES\Need for Speed Most Wanted\scripts` 
1. Copy savegame folder for Need for Speed Most Wanted (see `extra\savegame`) to `C:\Users\<username>\Documents\NFS Most Wanted`
1. Copy the file with saved positions from the game (`extra/HotPositionL2Ra.HOT`) to `"C:\Users\<username>\AppData\Local\VirtualStore\Program Files (x86)\EA GAMES\Need for Speed Most Wanted\TRACKS\HotPositionL2RA.HOT`
1. Install `python` packages with `pip install -r requirements.txt`

## Reading Data from the Game (Telemetry)

Data is being read from the game's memory directly using `pymem`. In order to track data:

**start the game:**
1. Start NFS:MW
1. Quick Race -> Custom Race -> Circuit
1. Heritage Heights (or any other)
1. Laps: 1 or more. (NFSMW ExtraOps required to have more than 6 laps)
1. Traffic Level: None
1. Opponents: 0
1. Difficulty: Any (irrelevant)
1. Catch Up: Any (irrelevant)
1. Transmission: Automatic (Manual does not give any advantage in the game)
1. Wait until the race has started and the 3-2-1 GO! has passed

**track telemetry:**
1. run `scripts/telemetry.py` to start tracking
1. to end tracking simply exit to the main menu
1. the script will write a `.csv`-file `%Y%m%d_%H%M%S_telemetry.csv`

Currently the following data can be tracked in real-time:

- `x, y, z (float)`: coordinates of the car in the game (like gps: lat, lng, elevation)
- `speed (float)`: in `m/s`
- `surface_l, surface_r (int)`: surface the car is driving on with the left/right wheels respectively
- `angle (int)`: direction of car
- `lap (int)`: current lap in a circuit race
- `laptime (float)`: current lap time in a circuit race
- `steering (float)`: steering input (left stick of Logitech RumblePad 2 gamepad)
- `throttle (float)`: throttle input (right stick of Logitech RumblePad 2 gamepad)
- `brake (int)`: brake input (shoulder button of Logitech RumblePad 2 gamepad)

## Train a Deep Reinforcement Learning Model

**start training script:**
1. Open `train_model.py` and run until `pad = VNfsGamePad()` which will launch a virtual Xbox360 Gamepad

**start the game:**
1. Start NFS:MW
1. Quick Race -> Custom Race -> Circuit
1. Heritage Heights (or any other)
1. Laps: 1 or more. (NFSMW ExtraOps required to have more than 6 laps)
1. Traffic Level: None
1. Opponents: 0
1. Difficulty: Any (irrelevant)
1. Catch Up: Any (irrelevant)
1. Transmission: Automatic (Manual does not give any advantage in the game)
1. Wait until the race has started and the 3-2-1 GO! has passed

**continue training script:**
1. Run the rest of `train_model.py`

This will train an reinforcement learning agent using the SAC algorithm. It will automatically save checkpoints of the model to a directory (deafult every 20k updates to the model).

## Test a Deep Reinforcement Learning Model

**start training script:**
1. Open `test_model.py` and run until `pad = VNfsGamePad()` which will launch a virtual Xbox360 Gamepad

**start the game:**
1. Start NFS:MW
1. Quick Race -> Custom Race -> Circuit
1. Heritage Heights (or any other)
1. Laps: 1 or more. (NFSMW ExtraOps required to have more than 6 laps)
1. Traffic Level: None
1. Opponents: 0
1. Difficulty: Any (irrelevant)
1. Catch Up: Any (irrelevant)
1. Transmission: Automatic (Manual does not give any advantage in the game)
1. Wait until the race has started and the 3-2-1 GO! has passed

**continue training script:**
1. Run the first part of `test_model.py` (`#do some random stuff`) to just test if everything is working
1. And/or run the second part to load a saved model and have the car drive by itself

This will test an reinforcement learning agent trained using the SAC algorithm.

## Structure of the Code

- `nfsmw.py` contains the base API to the NFS:MW game, that reads information and calculates features in real-time
- `env.py` contains an OpenAi gym environment for NFS:MW
- `train_model.py` and `test_model.py` are scripts to train and test models
- `control` contains classes and functions to send input to the game and take screenshots
- `data` contains a `reference_telemetry.csv` with trajectories for the racing line, the left and right border of the track, and "wrong" turns
- `scripts` contains some scripts to test functions from `nfsmw.py` and to create the `reference_telemetry.csv`
- `track` contains functions to read information from the game's memory

```
.
├── control
├── data
├── scripts
├── track
├── callback.py
├── env.py
├── nfsmw.py
├── test_model.py
└── train_model.py
```

